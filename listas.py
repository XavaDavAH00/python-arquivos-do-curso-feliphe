#Extender uma lista (exemplo 1):

convidados = ['Feliphe', 'Maria', 'Claudinei', 'Renata', 'Clarice']
while True:
    print(convidados)
    nome = input('Digite o nome do convidado: ')
    if nome.lower().__contains__("sair"): break
    convidados.extend([nome])
    #convidados += [nome]

#Extender uma lista (exemplo 2):
alfabeto = ['a', 'b', 'c', 'd', 'e', 'f']
print(alfabeto)
letras = "ghijklm"
alfabeto.extend(letras)
print(alfabeto)
letras = "nopqrst"
alfabeto.extend([letras])
print(alfabeto)
print("Extender a lista com: extends(\"string\") adicionara cada caractere da string")
print("como um elemento único na lista enquanto que extends([\"string\"]) adiciona")
print("toda a string como um único elemento na lista.", "\n")

#Observação sobre o append
print("A função .append() acrescenta um elemento ao final da lista.")
print("Equivalente a: lista.extend([elemento]) ou lista+=[elemento]", "\n")

# Inserindo elementos em uma lista:
lista = ["Elemento 1", "Elemento 2", "Elemento 3", "Elemento 4", "Elemento 5"]
print("A função .insert() insere um elemento em uma determinada posição da lista:")
print('Lista:', lista, ' inserindo \"Elemento x\" na posição 2:')
lista.insert(2, "Elemento x")
print("Lista pós-insersão:", lista, "\n")

# Removendo elementos de uma lista:
lista = ["Elemento 1", "Elemento 2", "Elemento 3", "Elemento 4", "Elemento 5"]
print("Lista:", lista)
print("Removendo último elemento:", lista.pop())
print("Lista:", lista)
print("Removendo primeiro elemento:", lista.pop(0))
print("Lista:", lista)

#Removendo elementos com a função DEL
print("Removendo último elemento da lista:", lista)
del(lista[-1]) #Se -1, remove ultimo. Senão, indice especificado.
print("Lista:", lista)

#Removendo elementos com a função REMOVE
print("Removendo último elemento da lista:", lista[-1])
lista.remove("Elemento 3")
print("Lista:", lista, "\n")

#Cópias linkadas e independentes
lista = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
lista2 = lista
lista3 = lista[:]
print("Lista 1:", lista, "\n")
print("Lista 2:", lista2, "\n")
print("Lista 3:", lista3, "\n")
print("As listas 1 e 2 são linkadas, portanto, ao adicionar ou remover elementos em")
print("alguma delas, a ação é refletida na outra lista:")
print("Comando -> lista.pop() -> ")
lista.pop()
print("Lista 1:", lista)
print("Lista 2:", lista2)
print("A lista 3, por ser independente, não sofreu alterações:")
print("Lista 3:", lista3, "\n")

#Contando ocorrências de elementos em uma lista
lista = ['A', 'E', 'I', 'O', 'U', 'A', 'A', 'A']
print("Lista:", lista)
print("Apareceram", str(lista.count("A")), "elementos A nessa lista.", "\n")

#Imprimindo a lista do fim para o começo
lista = ['A', 'E', 'I', 'O', 'U']
print("Lista:", lista)
lista.reverse()
print("Ao contrário:", lista)

#Obtendo o indice de um elemento da lista:
print("Obtendo a posição do elemento: A")
print("Indice:", lista.index("A"), "\n")

#Ordenar uma lista em ordem alfabética:
lista = ["Zebra", "Girafa", "Elefane", "Leão", "Andorinha", "Tatu", "Cobra"]
print("Lista:", lista)
# Parâmetros função sort:
lista.sort()
print("Em ordem alfabética:", lista, "\n")

#Imprimir com indice:
for i, elemento in enumerate(lista):
    print("Elemento: ", i+1, " - ", elemento)
print("\n")