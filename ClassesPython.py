# Classes em Python
# Orientação a objeto com Python

class Carros:
    def __init__(self, marca, modelo, carroceria, cor):
        self.marca = marca              # Honda, Toyota, Nissan, etc.
        self.modelo = modelo            # Accord, Civc, Skyline etc.
        self.carroceria = carroceria    # Hatch, Sedan, Coupé etc.
        self.cor = cor                  # Prata, Preto, Branco, etc.

    def get_car_info(self):
        print("Informações sobre o carro:")
        print("Marca:",         self.marca)
        print("Modelo:",        self.modelo)
        print("Carroceria:",    self.carroceria)
        print("Cor:",           self.cor)

# Ex. Herança ->
# class ExemploHerança(Carros):

print('Brincando com carros:')
carro = Carros(marca="Honda", modelo="Civic", carroceria="Sedan", cor="Prata")
carro.get_car_info()