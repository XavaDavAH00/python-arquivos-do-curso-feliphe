#Programa python para demonstrar manipulação de string

def aPrint(aStr, tab=4):
    print(aStr.rjust(len(aStr) + tab, ' '))

print('-'*100)
aPrint('Olá, mundo!')
aPrint('Este programa irá demonstrar o que Python é capaz de fazer com strings.')
print('-'*100, '\n')

#Len
nome = "feliphe"
aPrint('Com a função Len é possível saber o tamanho de uma string:')
aPrint('nome = \'Feliphe\' len(nome) retorna: ' + str(len(nome)))
print('\n')

#Capitalize
aPrint("Com a função capitalize é possível tonar a 1 letra de uma string maiúscula: ")
aPrint(nome + " -> " + nome.capitalize())
print('\n')

#Join
frase = ["Feliphe", "Lorrã", "gosta", "de", "programar", "!"]
aPrint('Com a função join é possível juntar duas strings:')
aPrint(" ".join(frase))
print('\n')

#LJust
frase = "Eu amo programação!"
aPrint('Com a função ljust é possível alinhar a string à esquerda e realizar espaçamento à direita:')
aPrint(frase.ljust(45)+"Nova frase após espaçamento.")
print('\n')

#RJust
frase = "Eu amo programação!"
aPrint('Com a função rjust é possível alinhar a string à direita e realizar espaçamento à esquerda:')
aPrint("Nova frase antes do espaçamento."+frase.rjust(45))
print('\n')

#Center
aPrint('Com a função Center uma string é centralizada na tela:')
print('-'*100)
print(str(nome.capitalize().center(100)))
print('-'*100)
print('\n')

#StartsWith
nome = "feliphe"
aPrint('Com a função StartsWith é possível verificar se uma string começa com uma substring')
aPrint('ou caractere específico:')
aPrint(nome.capitalize() + ' começa com \'Fe\' ? Resposta: ' + str(nome.startswith('fe')) + '(Verdadeiro)')
aPrint(nome.capitalize() + ' começa com \'Phe\' ? Resposta: ' + str(nome.startswith('phe')) + '(Falso)')
print('\n')

#EndsWith
aPrint('Com a função EndsWith é possível verificar se uma string termina com uma substring')
aPrint('ou caractere específico:')
aPrint(nome.capitalize() + ' termina com \'phe\' ? Resposta: ' + str(nome.endswith('phe')) + '(Verdadeiro)')
aPrint(nome.capitalize() + ' termina com \'pe\' ? Resposta: ' + str(nome.endswith('pe')) + '(Falso)')
print('\n')

#Count
nomes = ['Feliphe', 'Caleb', 'Alek', 'Feliphe', 'Caleb', 'Feliphe']
aPrint('Com a função Count é possível saber a quantidade de vezes que um elemento está')
aPrint('contido em uma lista:')
print('    Elementos da lista:', ' '.join(nomes))
aPrint('Nome Feliphe aparece: ' + str(nomes.count('Feliphe')) + " vezes.")
aPrint('Nome Caleb aparece: ' + str(nomes.count('Caleb')) + " vezes.")
aPrint('Nome Alek aparece: ' + str(nomes.count('Alek')) + " vezes.")
print('\n')

#Lower
nome = 'FELIPHE'
aPrint("Com a função .lower() é possível tornar os caracteres de uma string minúsculos:")
aPrint(nome + " -> " + nome.lower())
print('\n')

#Upper
nome = 'feliphe'
aPrint("Com a função .upper() é possível tornar os caracteres de uma string maiúsculos:")
aPrint(nome + " -> " + nome.upper())
print('\n')

#SwapCase
nome = 'FeLiPhE lOrRã SeRoDiO jEsUs'
aPrint("Com a função .swapcase() é possível inverter os caractéres maiúsculos em minúsculos e vice-versa:")
aPrint(nome + " -> " + nome.swapcase())
print('\n')

#IsTitle
nome = 'Feliphe Lorra Serodio Jesus'
name = 'feliphe lorra serodio jesus'
aPrint("Com a função .istitle() é possível verificar se cada inicial de uma palavra dentro da string")
aPrint("começa com letra maiúscula:")
aPrint("\"" + nome + "\".istitle() = " + str(nome.istitle()))
aPrint("\"" + name + "\".istitle() = " + str(name.istitle()))
print('\n')

#IsSpace
nome = 'feliphe'
aPrint("Com a função .isspace() é possível verificar se uma string é vazia ou não:")
aPrint("\"" + nome + "\".isspace() = " + str(nome.isspace()))
aPrint("\"       \".isspace() = " + str("       ".isspace()))
print('\n')

#IsNumeric
nome = 'feliphe'
aPrint("Com a função .isnumeric() é possível verificar se uma string é numérica ou não:")
aPrint("\"" + nome + "\".isnumeric() = " + str(nome.isnumeric()))
aPrint("\"1234567\".isnumeric() = " + str("1234567".isnumeric()))
print('\n')

#IsDecimal
nome = '224B'
aPrint("Com a função .isdecimal() é possível verificar se todos os caracteres de uma")
aPrint("string são decimais ou não:")
aPrint("\"" + nome + "\".isdecimal() = " + str(nome.isdecimal()))
aPrint("\"224\".isdecimal() = " + str("224".isdecimal()))
print('\n')

#IsAlNum
nome = '~~(^_^)~~'
aPrint("Com a função .isalnum() é possível verificar se uma string é alfa-numérica ou não:")
aPrint("\"" + nome + "\".isalnum() = " + str(nome.isalnum()))
aPrint("\"feliphe777\".isalnum() = " + str("feliphe777".isalnum()))
print('\n')

#IsAlpha
nome = 'feliphe'
aPrint("Com a função .isalpha() é possível verificar se uma string é composta somente")
aPrint("de caracteres alfabéticos ou não:")
aPrint("\"" + nome + "\".isalpha() = " + str(nome.isalpha()))
aPrint("\"feliphe777\".isalpha() = " + str("feliphe777".isalpha()))
print('\n')

#IsDigit
nome = 'feliphe'
aPrint("Com a função .isdigit() é possível verificar se uma string é um dígito ou não:")
aPrint("\"" + nome + "\".isdigit() = " + str(nome.isdigit()))
aPrint("\"1234567\".isdigit() = " + str("1234567".isdigit()))
print('\n')

#ExpandTabs
nome = 'Feliphe\tgosta\tde\tPython\t!!!'
aPrint("Com a função .expandtabs() é possível aumentar o espaço da tabulação de uma string:")
aPrint("String: "+'Feliphe\\tgosta\\tde\\tPython\\t!!!')
aPrint("Sem .expandtabs():  " + nome)
aPrint("Com .expandtabs(1): " + nome.expandtabs(1))
aPrint("Com .expandtabs(2): " + nome.expandtabs(2))
aPrint("Com .expandtabs(3): " + nome.expandtabs(3))
aPrint("Com .expandtabs(4): " + nome.expandtabs(4))
aPrint("Com .expandtabs(5): " + nome.expandtabs(5))
aPrint("Com .expandtabs(6): " + nome.expandtabs(6))
aPrint("Com .expandtabs(7): " + nome.expandtabs(7))
aPrint("Com .expandtabs():  " + nome.expandtabs())
aPrint("O parâmetro é opcional sendo que sem especificalo a tabulação será de 8 espaços.")
print('\n')

#Split
nome = "Feliphe Lorra Serodio Jesus"
frase = nome.split()
aPrint("Com a função split é possível dividir uma string em uma lista:")
aPrint("String: " + nome)
print("    string.split(): ", frase)
aPrint("Por padrão o caractere que separa é o espaço porém o mesmo pode ser especficado:")
nome = "FeliphexLorraxSerodioxJesus"
frase = nome.split("x")
aPrint("string: " + nome)
print("    string.split(x): ", frase)
aPrint("A função .rsplit() realiza a separação do fim pro começo:")
nome = "FeliphexLorraxSerodioxJesus"
frase = nome.rsplit("x")
aPrint("string: " + nome)
print("    string.rsplit(x): ", frase)
print("\n")

#Strip
nome = "AEIOUaeiou"
aPrint("Com a função Strip é possível remover caracteres ou substrings indesejados na string:")
aPrint("String: " + nome + ", string.strip(\"aeiou\"): " + nome.strip("aeiou"))
print("\n")

#ZFil
numero = "777"
aPrint("Com a função ZFill é possível preencher uma string com zeros à esquerda:")
aPrint("Número: " + numero + " com 7 casas numéricas: " + numero.zfill(7))