
# Forma errada
#def imprimeVariávelGlobal():
#    print(variávelGlobal)
#variávelGlobal = "Oi amigo."
#imprimeVariávelGlobal()

# Forma correta:
def imprimeVariávelGlobal():
    print(variávelGlobal)
global variávelGlobal
variávelGlobal = "Oi amigo."
imprimeVariávelGlobal()

