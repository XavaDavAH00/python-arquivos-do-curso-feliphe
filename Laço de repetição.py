#Laços de repetição

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
numeros = "0123456789"
for n in numeros:
    print("Número: ", n)
print("\n")
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
nomes = ['Maria', 'Feliphe', 'Eduardo', 'Jhenniffer', 'Renata', 'Claudinei', 'Caleb', 'Alek']
for n in nomes:
    print("Nome: ", n)
else:
    print("Fim da sequência de nomes.\n")
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
for n in range(11):
    print("Número: ", n)
print('\n')
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#TUPLAS - casais
nomes = [('Maria', 'Feliphe'), ('Jhenniffer', 'Eduardo'), ('Renata', 'Claudinei'), ('Amanda', 'Abner')]
for (mulher, homem) in nomes:
    print("Casal: ", mulher, " e ", homem)
else:
    print("Fim da sequência de casais.\n")

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
zero = False
pergunta = input('Deseja multiplicar por zero?\n')
if pergunta.lower().__contains__("sim") or pergunta.lower().__contains__("s"): zero = True
for n1 in range(11):
    if n1 == 0 and not zero: continue
    for n2 in range(11):
        if n2 == 0 and not zero: continue
        print(n1, ' x ', n2, ' = ', n1*n2)
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
n1 = 0
n2 = 0
zero = False
pergunta = input('Deseja multiplicar por zero?\n')
if pergunta.lower().__contains__("sim") or pergunta.lower().__contains__("s"): zero = True
while n1 <= 10:
    if n1 == 0 and not zero: n1 = n1 + 1
    while n2 <= 10:
        if n2 == 0 and not zero: n2 = n2 + 1
        print(n1, ' x ', n2, ' = ', n1 * n2)
        n2 = n2 + 1
    n1 = n1 + 1

