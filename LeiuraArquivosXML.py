from xml.etree.ElementTree import ElementTree

root = ElementTree(file='Carros.xml').getroot()
print(root.tag)

print(root.find(path='Carro').find(path='Marca').tag+":", root.find(path='Carro').find(path='Marca').text)
print(root.find(path='Carro').find(path='Modelo').tag+":", root.find(path='Carro').find(path='Modelo').text)
print(root.find(path='Carro').find(path='Tipo').tag+":", root.find(path='Carro').find(path='Tipo').text)
print(root.find(path='Carro').find(path='Cor').tag+":", root.find(path='Carro').find(path='Cor').text)
