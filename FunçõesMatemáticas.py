
#Função de potência:
print("Elevar: 2³ ->", pow(2,3))

#Função de arredondamento:
print("Arredondar: 2.6758 ->", round(2.6758))

#Somar números numa lista:
somar = [1,2,3,4,5,6,7,8,9,0]
print("Somar:", somar, sum(somar))

#Verificar maior elemento numa lista numérica:
lista = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
print("Lista:", lista, "Maior:", max(lista))

#Verificar menor elemento numa lista numérica:
lista = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
print("Lista:", lista, "Menor:", min(lista))