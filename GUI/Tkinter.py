from tkinter import *

# Classe
class Tela(Frame):
    def __init__(self, master = None):
        Frame.__init__(self, master)
        self.pack()

# Código
app = Tela()
app.master.title = "Primeira tela em Tkinter - Python"
app.master.maxsize(450, 330)
app.master.mainloop()
