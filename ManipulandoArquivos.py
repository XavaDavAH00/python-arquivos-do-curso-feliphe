#Abrir arquivo:

nome = ""
arquivo = None
while nome == "":
    nome = input("Digite o nome do arquivo com a extensão:")
try:
    print("Abrindo arquivo %s ..." % nome)
    arquivo = open(nome, "r")
    print("Arquivo %s aberto em modo %s." %(arquivo.name, arquivo.mode))
    print("Conteúdo do arquivo:")
    for linha in arquivo.readlines():
        print(linha)
except FileNotFoundError:
    print("O arquivo especificado não foi encontrado.")
    resp = input("Deseja criar o arquivo? (s/n) Resposta: ")
    if resp.lower() == "s":
        arquivo = open(nome, "w")
        arquivo.writelines("Este arquivo foi criado automaticamente usando PythonScript.")
        print("Arquivo gravado.")
except:
    print("Ops, algo deu errado.")
finally:
    try:
        if arquivo.closed == False:
            print("Fechando arquivo...")
            arquivo.flush() # Forçando dados em buffer serem gravados.
            arquivo.close()
            if arquivo.closed == True:
                print("Arquivo fechado com sucesso.")
    except:
        pass


