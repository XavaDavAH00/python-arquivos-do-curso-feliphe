import sys
import time
import types
import random
import string
import datetime
from os import getcwd
from os import listdir

# Usando números randômicos 1 - randint()
random.seed(random.randint(1, 1000))
count = 0
while True:
    nu1 = random.randint(1, 100)
    nu2 = random.randint(1, 100)
    print("Números pseudo-randômicos:", nu1, "e", nu2)
    if nu1 == nu2: break
    else: count+=1
print("Tentativas até serem iguais:", count, "\n")

# Usando números randômicos 2 - random()
print("random.random():", random.random(), "- números aleatórios entre 0 e 1." , "\n")

# Usando números randômicos 3 - uniform()
print("random.uniform():", random.uniform(1, 100), "- números aleatórios de ponto flutuante entre a e b." , "\n")

# Embaralhando elementos duma lista com shuffle()
lista = [1, 2, 3, 4, 5, 6, 7]
print("Lista (antes): ", lista)
random.shuffle(lista)
print("Lista (depois):", lista, "\n")

# Escolhendo caracteres aleatórios com random e string:
lista = []
for x in range(100):
    if x % 5 == 0:
        lista.extend("\n")
    lista.extend(random.choice(string.ascii_letters))
else:
# Módulo Sys
    sys.stdout.writelines(lista)
    print("\n")

# Módulo de tempo
print("time.localtime() ->", time.localtime())
hor = time.localtime().tm_hour
min = time.localtime().tm_min
seg = time.localtime().tm_sec
dia = time.localtime().tm_mday
mes = time.localtime().tm_mon
ano = time.localtime().tm_year
print("Hora e data: %d:%d:%d, %d/%d/%d." % (hor, min, seg, dia, mes, ano))
print("AscTime: %s" % time.asctime())

# Sleep
print("Sleep faz esperar a quantidade de segundos determinada:", "\nsleep(5) ->")
for s in range(5):
    print(int(s)+1)
    time.sleep(1)
else:
    print("Então, 5 segundos se passaram.", "\n")

# Usando datetime
datahora = datetime.datetime(hour=hor, minute=min, second= seg, day=dia, month=mes, year=ano)
print("Data:", datahora.date(), "Hora:", datahora.time(), "\n")

# Corrigindo a data
dData = str(datahora.date())
print("dData:", dData)
zData = dData.split('-')
print("zData:", zData)
zData.reverse()
print("zData:", zData)

# Mostrando o tipo de cada variável
def getTypeOf(array):
    _types = []
    for element in array:
        _types.extend("%s %s %s\n" % (element, "->", type(element)))
    return _types

sys.stdout.writelines(getTypeOf([1, 1.10, True, 'Olá!', ['a'], print, getTypeOf]))

def getTypeOfFunction(thing):
    if type(thing) == types.BuiltinFunctionType:
        print("Função nativa.")
    elif type(thing) == types.FunctionType:
        print("Função construída.")

getTypeOfFunction(print)
getTypeOfFunction(getTypeOf)

print("Função getcwd retorna o diretório atual:")
print(getcwd())
print("Função listdir lista o conteúdo contido no diretório especificado:")
print("listdir('.idea'):\n", listdir('.idea'))
print("Quando o parâmetro especificado for um '.', retorna uma lista de tudo contido no diretório atual:")
print(listdir('.'))

