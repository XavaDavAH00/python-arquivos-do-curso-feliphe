# Funções em python

def somar(num1, num2):
    print("Somando %d + %d: %d" % (num1, num2, (num1+num2)))
    return num1+num2
somar(1,2)

#Empacotamento e desempacotamento de parâmetros
def somar2(*numeros):
    s = 0
    for n in numeros:
        s += n
    return s

numeros = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
valor = somar2(*numeros)
print(valor)
# EXPRESSÃO LAMBDA
soma3 = lambda num1, num2: num1+num2
print(soma3(1, 2))