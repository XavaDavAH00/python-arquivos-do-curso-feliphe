# Usar comando: pip install mysql-connector-python
from mysql import connector
from mysql.connector import errorcode
from termcolor import cprint, colored

try:
    conn = connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="bd_carros"
    )
except connector.Error as erro:
    if erro.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print(colored(text='ERRO:', color='red', attrs=['bold']),
              colored(text='Usuário e/ou senha inválido(s).', color='red'))
    elif erro.errno == errorcode.ER_BAD_DB_ERROR:
        print(colored(text='ERRO:', color='red', attrs=['bold']),
              colored(text='O banco de dados especificado não existe.', color='red'))
    else:
        print(colored(text='ERRO:', color='red', attrs=['bold']),
              colored(text=erro.msg, color='red'))
    exit(erro.errno)

cur = conn.cursor()
#   query = """insert into carros (marca, modelo, tipo, cor, cambio, placa)
#   values ('Honda', 'Civic', 'Sedan', 'Branco', 'Manual', 'FEL-0001')"""
#   cur.execute(query)
#   query = 'update carros set portas = \'4\' where id = \'5\''
#   cur.execute(query)
#   query = 'delete from carros where id = \'1\''
#   cur.execute(query)
#   ->  Usar o commit após as operações ->
#   conn.commit()

query = 'select * from carros'
cur.execute(query)
result = cur.fetchall()
conn.close()
cprint(text=query.upper(), color='cyan', attrs=['bold'])
for l in result:
    print(colored('QueryResult:', color='green', attrs=['bold']), colored(l, 'green'))
