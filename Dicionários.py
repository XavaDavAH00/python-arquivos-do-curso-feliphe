# Tipo de dados: dicionário

notas = {
    (0, 1, 2):  "Péssimo",
    (3, 4):     "Ruim",
    5:          "Regular",
    (6, 7):     "Bom",
    (8, 9):     "Ótimo",
    10:         "Excelente"
}
materias = {
    1: ["Português",     "Fundamental 2", 250],
    2: ["Matemática",    "Fundamental 2", 250],
    3: ["História",      "Fundamental 2", 200],
    4: ["Geografia",     "Fundamental 2", 200],
    5: ["Ciências",      "Fundamental 2", 250],
    6: ["Artes",         "Fundamental 2", 50]
}
portugues = {
    True: "Verdadeiro",
    False: "Falso"
}

print("Keys:", str(notas.keys()))
print("Values:", str(notas.values()))
print("Valor na key 5:", notas[5])
print("O valor Feliphe existe no dicionário?", portugues[("Feliphe" in notas)])
print(portugues.items())

# Exemplo de como percorrer os dados de um dicionário:
for (key, array) in materias.items():
    print("Chave: %d - Matéria: %s, carga horária: %d." %(int(key), array[0], int(array[2])))

# Métodos do tipo de dados dicionário: #

#Método: clear e método copy
dicionario = {
    1:["Um"     ,   "Primeiro"  ],
    2:["Dois"   ,   "Segundo"   ],
    3:["Tês"    ,   "Terceiro"  ],
    4:["Quatro" ,   "Quarto"    ]
}
print("Valores do dicionário antes do método clear ser execultado:")
for (chave, valor) in dicionario.items():
    print("Chave: %d, valor: %s (%s)" %(chave, valor[0], valor[1]))
else:
    #Realizando a cópia do dicionário antes de apagar tudo.
    copiaDicionario = dicionario.copy()
    dicionario.clear()
print("Valores do dicionário após a execução do método clear:")
print("Tamanho:", len(dicionario.items()), "Valores:", dicionario.values())
print("Valores do dicionário de cópia:")
for (chave, valor) in copiaDicionario.items():
    print("Chave: %d, valor: %s (%s)" %(chave, valor[0], valor[1]))

# Método get - pegando valores de uma key especificada:
print("Valores:", portugues.get(True), portugues.get(False))
valor = materias.get(1)
print("V1: %s, V2: %s, V3: %d." % (array[0], array[1], int(array[2])), "\n")

# Método setdefault - adiciona o valor ao dicionário, caso não exista.
print("Método setdefault adiciona o valor ao dicionário caso ele não exista.")
print("Valores de matérias:", materias.items())
print("Procurando matéria de key 7 com get:", materias.get(7))
print("Procurando matéria de key 7 com setdefault:", materias.setdefault(7, ["Python intermediário", "Extra-curricular", 250]))
print("Valores de matérias:", materias.items())