#Script python que demostra o controle de fluxo com IF, ELIF E ELSE

nota = input("Digite a sua nota: ")
if int(nota) > 6:
    print("Parabéns, você passou!")
elif int(nota) == 6:
    print("Ops, você ficará de recuperação.")
else:
    print("Você repetiu...")

